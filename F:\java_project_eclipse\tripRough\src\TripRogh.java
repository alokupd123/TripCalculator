import java.awt.Color;
import java.awt.FlowLayout;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import net.miginfocom.swing.MigLayout;

public class TripRogh extends JFrame {
	//frame objects
	ImageIcon img = new ImageIcon("F:/java_project/jficon.jpg");
	
	//tabbed pane objects
    JTabbedPane tabpane=new JTabbedPane(); //tabbed panel object for all tabs
    
    
   
	//add new trip objects
    JPanel panelAddNewTrip=new JPanel();               //panel add new trip
	JLabel lblTripTitle=new JLabel("ADD_NEW_TRIP");   
	JLabel lblTripID=new JLabel("TRIP_ID");
	JTextField txtTripID=new JTextField(15);
	JLabel lblTripName=new JLabel("TRIP_NAME");
	JTextField txtTripName=new JTextField(15);
	JLabel lblStartDate=new JLabel("START_DATE");
	JTextField txtStartDate=new JTextField(15);
	JLabel lblEndDate=new JLabel("END_DATE");
	JTextField txtEndDate=new JTextField(15);
	JLabel lblExpBudget=new JLabel("EXPECTED_BUDGET");
	JTextField txtExpBudget=new JTextField(15);
	JLabel lblerrMsgTrip=new JLabel("");
	
	JButton btnSaveTrip=new JButton("SAVE");                 //button object for add new trip panel
	JButton btnModifyTrip=new JButton("MODIFY");
	JButton btnDeleteTrip=new JButton("DELETE");
	
	//add new member objects
    JPanel panelAddNewMembr=new JPanel();             
	
	JLabel lblMembrTitle=new JLabel("ADD_NEW_MEMBER");    
	JLabel lblMembrID=new JLabel("MEMBER_ID");
	JTextField txtMembrID=new JTextField(15);
	JLabel lblMembrName=new JLabel("MEMBER_NAME");
	JTextField txtMembrName=new JTextField(15);
	JLabel lblMembrContact=new JLabel("MEMBER_CONTACT_NO");
	JTextField txtMembrContact=new JTextField(15);
	JLabel lblEmail=new JLabel("EMAIL_ID");
	JTextField txtEmail=new JTextField(15);
	JLabel lblAddress=new JLabel("ADDRESS");
	JTextField txtAddress=new JTextField(15);
	JLabel lblerrMsgMembr=new JLabel("");
	
	JButton btnSaveMembr=new JButton("SAVE");                 
	JButton btnModifyMembr=new JButton("MODIFY");
	JButton btnDeleteMembr =new JButton("DELETE");
	
	//trip member objects
    JSplitPane splitPaneTripMembr = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    
	JPanel splitPaneTripMembrLeft = new JPanel();
	JLabel lblTripMembr=new JLabel("TRIP_MEMBERS");
	JLabel lblTripMembrMID=new JLabel("MEMBER_ID");
	JComboBox comboBoxMembrID = new JComboBox();
	JLabel lblTripMembrTID=new JLabel("TRIP_ID");
	JComboBox comboBoxTripID = new JComboBox();
	JLabel lblTripMembrMName=new JLabel("MEMBER_NAME");
	JTextField txtTripMembrMName=new JTextField(15);
	JLabel lblTripMembrTName=new JLabel("TRIP_NAME");
	JTextField txtTripMembrTName=new JTextField(15);
	JLabel lblerrMsgTripMembr=new JLabel("");
	
	
	JScrollPane splitPaneTripMembrRight = new JScrollPane();
	JPanel panelTripMembrResult=new JPanel();
	String data[][]={ { "101","RAM","123"},
						{"102","shyam","456"},
						{"103","alok","789"}};
	
	String column[]={"id","name","amt"};
	JTable table=new JTable(data,column);
		
	JButton btnSaveTripMembr=new JButton("SAVE");                 
	JButton btnModifyTripMembr=new JButton("MODIFY");
	JButton btnDeleteTripMembr=new JButton("DELETE");
	
	//Money Deposit objects
	  
	JSplitPane splitPaneMoneyDeposit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
	    
	JPanel splitPaneMoneyDepositLeft = new JPanel();
    JLabel lblMoneyDeposit=new JLabel("MONEY_DEPOSIT");
    JLabel lblDepositTripID=new JLabel("TRIP_ID");
    JComboBox comboDepositTripID=new JComboBox();
    JLabel lblDepositTripName=new JLabel("TRIP_NAME");
    JTextField txtDepositTripName=new JTextField(15);
    JLabel lblDepositMembrID=new JLabel("MEMBER_ID");
    JComboBox comboDepositMembrID=new JComboBox();
    JLabel lblDepositMembrName=new JLabel("MEMBER_NAME");
    JTextField txtDepositMembrName=new JTextField(15);
    JLabel lblDepositAmt=new JLabel("DEPOSIT_AMOUNT");
    JTextField txtDepositAmt=new JTextField(15);
    JLabel lblDepositDate=new JLabel("DEPOSIT_DATE");
    JTextField txtDepositDate=new JTextField(15);
	JScrollPane splitPaneMoneyDepositRight = new JScrollPane();
			
	JButton btnSaveDeposit=new JButton("SAVE");                 
	JButton btnModifyDeposit=new JButton("MODIFY");
	JButton btnDeleteDeposit=new JButton("DELETE");
	
	//expenditure panel objects
	JPanel panelExpend=new JPanel();
	JLabel lblExpendTitle=new JLabel("EXPENDITURE");
	JLabel lblExpendDate=new JLabel("EXPENDITURE_DATE");
	JTextField txtExpendDate=new JTextField(15);
	JLabel lblExpendAmt=new JLabel("EXPENDITURE_AMOUNT");
	JTextField txtExpendAmt=new JTextField(15);
	JLabel lblExpendDescription=new JLabel("EXPENDITURE_DESCRIPTION");
	JTextArea txtExpendDescription=new JTextArea(5,5);
	
	JButton btnSaveExpend=new JButton("SAVE");                 
	JButton btnModifyExpend=new JButton("MODIFY");
	JButton btnDeleteExpend=new JButton("DELETE");
	
	
    
    public TripRogh()      
    {
    	Connection c = null;
        Statement stmt = null;
    // add new trip panel properties and functionality
    panelAddNewTrip.setLayout(new MigLayout());
    panelAddNewTrip.setBackground(Color.LIGHT_GRAY);
    panelAddNewTrip.add(lblTripTitle, "cell 2 0");
    panelAddNewTrip.add(lblTripID, "cell 0 2");
    panelAddNewTrip.add(txtTripID,"cell 2 2");
    panelAddNewTrip.add(lblTripName, "cell 0 4");
    panelAddNewTrip.add(txtTripName, "cell 2 4");
    panelAddNewTrip.add(lblStartDate, "cell 0 6");
    panelAddNewTrip.add(txtStartDate, "cell 2 6");
    panelAddNewTrip.add(lblEndDate, "cell 0 8");
    panelAddNewTrip.add(txtEndDate, "cell 2 8");
    panelAddNewTrip.add(lblExpBudget, "cell 0 10");
    panelAddNewTrip.add(txtExpBudget, "cell 2 10");
    panelAddNewTrip.add(btnSaveTrip, "cell 6 22");
    panelAddNewTrip.add(btnModifyTrip, "cell 10 22");
    panelAddNewTrip.add(btnDeleteTrip, "cell 14 22");
    panelAddNewTrip.add(lblerrMsgTrip);
    btnModifyTrip.setEnabled(false);
    btnDeleteTrip.setEnabled(false);
    
  //actionlistener and database connectivity for Add new trip panel(btnSaveTrip+btnModifyTrip+btnDeleteTrip)
    btnSaveTrip.addActionListener(new ActionListener(){
    	public void actionPerformed(ActionEvent arg0){
    		Connection c = null;
    	    Statement stmt = null;
    		try {
    		     Class.forName("org.sqlite.JDBC");
    		     c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
    		     System.out.println("Opened database successfully");
    		     stmt = c.createStatement();
    		     String sql ="Insert Into tbladdtrip Values('" + txtTripID.getText() + "','" + txtTripName.getText() +"','" + txtStartDate.getText() +"','" + txtEndDate.getText() +"','" + txtExpBudget.getText() +"')"; 
    		     stmt.executeUpdate(sql);
    		     stmt.close();
    		     c.close();
    		    } catch ( Exception e ) {
    		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    		      
    		    }
    		    lblerrMsgTrip.setText("TRIP_ADDED");
    		    txtTripID.setText("");
    		    txtTripName.setText("");
    		    txtStartDate.setText("");
    		    txtEndDate.setText("");
    		    txtExpBudget.setText("");
    		    
    		  }
    		
    		
  
	});
    txtTripID.addFocusListener(new FocusListener(){
        @Override
		public void focusGained(FocusEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void focusLost(FocusEvent e) {
			Connection c = null;
    	    Statement stmt = null;
			try
			{
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
				stmt = c.createStatement();
   		     ResultSet rs=stmt.executeQuery("Select * From tbladdtrip Where TRIPID='" + txtTripID.getText() + "'");
				if(rs.next()) //found
				{
					txtTripName.setText(rs.getString("tripname"));
					txtStartDate.setText(rs.getString("startdate"));
					txtEndDate.setText(rs.getString("enddate"));
					txtExpBudget.setText(rs.getString("expbudget"));
					btnSaveTrip.setEnabled(false);
					btnModifyTrip.setEnabled(true);
					btnDeleteTrip.setEnabled(true);
				}
				else
				{
					txtTripName.setText("");
					btnSaveTrip.setEnabled(true);
					btnModifyTrip.setEnabled(false);
					btnDeleteTrip.setEnabled(false);
				}
				stmt.close();
				c.close();
			}catch(Exception ee){
				lblerrMsgTrip.setText(ee.toString());
			}

			
		}
		
	});
    
    btnModifyTrip.addActionListener(new ActionListener(){
    	public void actionPerformed(ActionEvent arg0){
    		Connection c = null;
    	    Statement stmt = null;
    		try
			{
    			Class.forName("org.sqlite.JDBC");
    			c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
				stmt = c.createStatement();
				int n=stmt.executeUpdate("Update tbladdtrip set tripname='" + txtTripName.getText() + "',startdate='" + txtStartDate.getText() + "',enddate='" + txtEndDate.getText() + "',expbudget='" + txtExpBudget.getText() + "' Where tripid='" + txtTripID.getText() +"'");
				if(n==1){
						lblerrMsgTrip.setText("ENTRY MODIFIED");
						txtTripID.setText("");
						txtTripName.setText("");
						txtStartDate.setText("");
						txtEndDate.setText("");
						txtExpBudget.setText("");
						
				}
				else
			    lblerrMsgTrip.setText("Entry Not Modified");
				//c.close();
				stmt.close();
			}catch(Exception ee){
				lblerrMsgTrip.setText(ee.toString());
			}
    		    
    	
    	
}
    		
    		
    			
    });
    
    btnDeleteTrip.addActionListener(new ActionListener(){
    	public void actionPerformed(ActionEvent arg0){
    		Connection c = null;
    	    Statement stmt = null;
    		try
			{
    			Class.forName("org.sqlite.JDBC");
    			c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
				stmt = c.createStatement();
				int n=stmt.executeUpdate("delete from tbladdtrip Where tripid='" + txtTripID.getText() +"'");
				
				//c.close();
				stmt.close();
			}catch(Exception ee){
				lblerrMsgTrip.setText(ee.toString());
			}
    		lblerrMsgTrip.setText("ENTRY DELETED");
			txtTripID.setText("");
			txtTripName.setText("");
			txtStartDate.setText("");
			txtEndDate.setText("");
			txtExpBudget.setText("");
    	   
    		
    			
    		
    		
    		}

	});
    
    
    //add member panel properties and functionality
    panelAddNewMembr.setLayout(new MigLayout());
    panelAddNewMembr.setBackground(Color.LIGHT_GRAY);
    panelAddNewMembr.add(lblMembrTitle, "cell 2 0");
    panelAddNewMembr.add(lblMembrID, "cell 0 2");
    panelAddNewMembr.add(txtMembrID,"cell 2 2");
    panelAddNewMembr.add(lblMembrName, "cell 0 4");
    panelAddNewMembr.add(txtMembrName, "cell 2 4");
    panelAddNewMembr.add(lblMembrContact, "cell 0 6");
    panelAddNewMembr.add(txtMembrContact, "cell 2 6");
    panelAddNewMembr.add(lblEmail, "cell 0 8");
    panelAddNewMembr.add(txtEmail, "cell 2 8");
    panelAddNewMembr.add(lblAddress, "cell 0 10");
    panelAddNewMembr.add(txtAddress, "cell 2 10");
    panelAddNewMembr.add(btnSaveMembr, "cell 6 22");
    panelAddNewMembr.add(btnModifyMembr, "cell 10 22");
    panelAddNewMembr.add(btnDeleteMembr, "cell 14 22");
    panelAddNewMembr.add(lblerrMsgMembr);
    btnModifyMembr.setEnabled(false);
    btnDeleteMembr.setEnabled(false);
    
    
  //actionlistener and database connectivity for Add new member panel(btnSaveMembr+btnModifyMembr+btnDeletemembr)
    
    btnSaveMembr.addActionListener(new ActionListener(){
    	public void actionPerformed(ActionEvent arg0){
    	   
    		Connection c = null;
    	    Statement stmt = null;
    		try {
    		     Class.forName("org.sqlite.JDBC");
    		     c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
    		     System.out.println("Opened database successfully");
    		     stmt = c.createStatement();
    		     String sql ="Insert Into tbladdmembr Values('" + txtMembrID.getText() + "','" + txtMembrName.getText() +"','" + txtMembrContact.getText() +"','" + txtEmail.getText() +"','" + txtAddress.getText() +"')"; 
    		     stmt.executeUpdate(sql);
    		     stmt.close();
    		     c.close();
    		    } catch ( Exception e ) {
    		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    		      
    		    }
    		    lblerrMsgMembr.setText("MEMBER_ADDED");
    		    txtMembrID.setText("");
    		    txtMembrName.setText("");
    		    txtMembrContact.setText("");
    		    txtEmail.setText("");
    		    txtAddress.setText("");
    		    
    			
    		
    		
    		}
    	

	});
    txtMembrID.addFocusListener(new FocusListener(){
        @Override
		public void focusGained(FocusEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void focusLost(FocusEvent e) {
			Connection c = null;
    	    Statement stmt = null;
			try
			{
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
				stmt = c.createStatement();
   		     ResultSet rs=stmt.executeQuery("Select * From tbladdmembr Where membrid='" + txtMembrID.getText() + "'");
				if(rs.next()) //found
				{
					txtMembrName.setText(rs.getString("membrname"));
					txtMembrContact.setText(rs.getString("membrcntct"));
					txtEmail.setText(rs.getString("membremail"));
					txtAddress.setText(rs.getString("membraddr"));
					btnSaveMembr.setEnabled(false);
					btnModifyMembr.setEnabled(true);
					btnDeleteMembr.setEnabled(true);
				}
				else
				{
					txtMembrName.setText("");
					btnSaveMembr.setEnabled(true);
					btnModifyMembr.setEnabled(false);
					btnDeleteMembr.setEnabled(false);
				}
				stmt.close();
				c.close();
			}catch(Exception ee){
				lblerrMsgTrip.setText(ee.toString());
			}

			
		}
		
	});
    
    btnModifyMembr.addActionListener(new ActionListener(){
    	public void actionPerformed(ActionEvent arg0){
    		Connection c = null;
    	    Statement stmt = null;
    		try
			{
    			Class.forName("org.sqlite.JDBC");
    			c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
				stmt = c.createStatement();
				int n=stmt.executeUpdate("Update tbladdmembr set membrname='" + txtMembrName.getText() + "',membrcntct='" + txtMembrContact.getText() + "',membremail='" + txtEmail.getText() + "',membraddr='" + txtAddress.getText() + "' Where membrid='" + txtMembrID.getText() +"'");
				if(n==1){
						lblerrMsgMembr.setText("ENTRY MODIFIED");
						txtMembrID.setText("");
						txtMembrName.setText("");
						txtMembrContact.setText("");
						txtEmail.setText("");
						txtAddress.setText("");
						
				}
				else
			    lblerrMsgMembr.setText("Entry Not Modified");
				//c.close();
				stmt.close();
			}catch(Exception ee){
				lblerrMsgTrip.setText(ee.toString());
			}
    		    
    		
    		}

	});
    
    btnDeleteMembr.addActionListener(new ActionListener(){
    	public void actionPerformed(ActionEvent arg0){
    		Connection c = null;
    	    Statement stmt = null;
    		try
			{
    			Class.forName("org.sqlite.JDBC");
    			c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
				stmt = c.createStatement();
				int n=stmt.executeUpdate("delete from tbladdmembr Where membrid='" + txtMembrID.getText() +"'");
				
				//c.close();
				stmt.close();
			}catch(Exception ee){
				lblerrMsgMembr.setText(ee.toString());
			}
    		lblerrMsgMembr.setText("ENTRY DELETED");
			txtMembrID.setText("");
			txtMembrName.setText("");
			txtMembrContact.setText("");
			txtEmail.setText("");
			txtAddress.setText("");
    	   
    		
    			
    		
    		
    		}

	});
   
   
    
    //Trip members properties And Functionality
    splitPaneTripMembrLeft.setLayout(new MigLayout());
    splitPaneTripMembrLeft.setBackground(Color.LIGHT_GRAY);
    splitPaneTripMembrLeft.add(lblTripMembr,"cell 2 0");
    splitPaneTripMembrLeft.add(lblTripMembrTID,"cell 0 1");
    splitPaneTripMembrLeft.add(lblerrMsgTripMembr,"cell 18 18");
    txtTripMembrTName.setEnabled(false);
    txtTripMembrMName.setEnabled(false);
    
    //add tripid to combobox tripid from table tbltrip
    try
	{
		
		Class.forName("org.sqlite.JDBC");
		c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
		stmt = c.createStatement();
	     ResultSet rs=stmt.executeQuery("Select tripid From tbladdtrip");
		while(rs.next()) //found
		{
			 comboBoxTripID.addItem(rs.getString("tripid"));
			
		}
		
		stmt.close();
		c.close();
	}catch(Exception ee){
		System.out.println(ee.toString());
	}
    
    //
    
	comboBoxTripID.addItemListener(new ItemListener(){

		@Override
		public void itemStateChanged(ItemEvent e) {
			Connection c = null;
    	    Statement stmt = null;
			try
			{
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
				stmt = c.createStatement();
   		     ResultSet rs=stmt.executeQuery("Select * From tbladdtrip Where TRIPID='" + comboBoxTripID.getItemAt(comboBoxTripID.getSelectedIndex()) + "'");
				if(rs.next()) //found
				{
					txtTripMembrTName.setText(rs.getString("tripname"));
					
				}
				else
				{
					txtTripMembrTName.setText("");
					
				}
				stmt.close();
				c.close();
			}catch(Exception ee){
				System.out.println(ee.toString());
			}
			
			
			
			
			
			
			
			
			
			
			
			
		}
		
	});
    
    
    
    
    
   
    splitPaneTripMembrLeft.add(comboBoxTripID,"cell 2 1");
    comboBoxTripID.setMaximumRowCount(20);
    splitPaneTripMembrLeft.add(lblTripMembrTName,"cell 0 4");
    splitPaneTripMembrLeft.add(txtTripMembrTName,"cell 2 4");
    splitPaneTripMembrLeft.add(lblTripMembrMID,"cell 0 6");
    splitPaneTripMembrLeft.add(comboBoxMembrID,"cell 2 6");
    comboBoxMembrID.setMaximumRowCount(20);
    splitPaneTripMembrLeft.add(lblTripMembrMName,"cell 0 7");
    splitPaneTripMembrLeft.add(txtTripMembrMName,"cell 2 7");
    try
	{
		
		Class.forName("org.sqlite.JDBC");
		c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
		stmt = c.createStatement();
	     ResultSet rs=stmt.executeQuery("Select membrid From tbladdmembr");
		while(rs.next()) //found
		{
			 comboBoxMembrID.addItem(rs.getString("membrid"));
			
		}
		
		stmt.close();
		c.close();
	}catch(Exception ee){
		System.out.println(ee.toString());
	}
    
    //
    
	comboBoxMembrID.addItemListener(new ItemListener(){

		@Override
		public void itemStateChanged(ItemEvent e) {
			Connection c = null;
    	    Statement stmt = null;
			try
			{
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
				stmt = c.createStatement();
   		     ResultSet rs=stmt.executeQuery("Select * From tbladdmembr Where membrID='" + comboBoxMembrID.getItemAt(comboBoxMembrID.getSelectedIndex()) + "'");
				if(rs.next()) //found
				{
					txtTripMembrMName.setText(rs.getString("membrname"));
					
				}
				else
				{
					txtTripMembrMName.setText("");
					
				}
				stmt.close();
				c.close();
			}catch(Exception ee){
				System.out.println(ee.toString());
			}
			
			
		}
		
	});
    
    
    
    
    
    
   
    splitPaneTripMembrLeft.add(btnSaveTripMembr,"cell 14 1");
    splitPaneTripMembrLeft.add(btnModifyTripMembr,"cell 14 5");
    splitPaneTripMembrLeft.add(btnDeleteTripMembr,"cell 14 7");
    splitPaneTripMembrRight.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    splitPaneTripMembrRight.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    splitPaneTripMembrRight.setBackground(Color.LIGHT_GRAY);
    splitPaneTripMembr.setLeftComponent(splitPaneTripMembrLeft);
    
    splitPaneTripMembrRight.getViewport().add(table);
    splitPaneTripMembr.setRightComponent(splitPaneTripMembrRight);
    
    //add trip membr Action listener and database connectivity
    
   btnSaveTripMembr.addActionListener(new ActionListener(){
    	public void actionPerformed(ActionEvent arg0){
    		Connection c = null;
    	    Statement stmt = null;
    		try {
    		     Class.forName("org.sqlite.JDBC");
    		     c = DriverManager.getConnection("jdbc:sqlite:tripcalc.db");
    		     System.out.println("Opened database successfully");
    		     stmt = c.createStatement();
    		     String sql ="Insert Into tbltripmembr Values('" + comboBoxTripID.getItemAt(comboBoxTripID.getSelectedIndex()) + "','" + txtTripMembrTName.getText() +"','" + comboBoxMembrID.getItemAt(comboBoxMembrID.getSelectedIndex()) + "','" + txtTripMembrMName.getText() +"')"; 
    		     stmt.executeUpdate(sql);
    		     stmt.close();
    		     c.close();
    		    } catch ( Exception e ) {
    		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    		      
    		    }
    		    lblerrMsgTripMembr.setText("TRIP_MEMBER_ADDED");
    		   
    		    
    		  }
    		
    			
    		
    		
    		

	});
    
    btnModifyTripMembr.addActionListener(new ActionListener(){
    	public void actionPerformed(ActionEvent arg0){
    	   
    		
    			
    		
    		
    		}

	});
    
    btnDeleteTripMembr.addActionListener(new ActionListener(){
    	public void actionPerformed(ActionEvent arg0){
    	   lblTripTitle.setText("kuch to hua9");
    		
    			
    		
    		
    		}

	});
   
   //money deposite properties and functionality
    splitPaneMoneyDepositLeft.setLayout(new MigLayout());
    splitPaneMoneyDepositLeft.setBackground(Color.LIGHT_GRAY);
    splitPaneMoneyDepositLeft.add(lblMoneyDeposit,"cell 2 0");
    splitPaneMoneyDepositLeft.add(lblDepositTripID,"cell 0 2");
    splitPaneMoneyDepositLeft.add(comboDepositTripID,"cell 2 2");
    comboBoxTripID.setMaximumRowCount(20);
    splitPaneMoneyDepositLeft.add(lblDepositTripName,"cell 0 4");
    splitPaneMoneyDepositLeft.add(txtDepositTripName,"cell 2 4");
    splitPaneMoneyDepositLeft.add(lblDepositMembrID,"cell 0 6");
    splitPaneMoneyDepositLeft.add(comboDepositMembrID,"cell 2 6");
    comboBoxMembrID.setMaximumRowCount(20);
    splitPaneMoneyDepositLeft.add(lblDepositMembrName,"cell 0 8");
    splitPaneMoneyDepositLeft.add(txtDepositMembrName,"cell 2 8");
    splitPaneMoneyDepositLeft.add(lblDepositAmt,"cell 0 10");
    splitPaneMoneyDepositLeft.add(txtDepositAmt,"cell 2 10");
    splitPaneMoneyDepositLeft.add(lblDepositDate,"cell 0 12");
    splitPaneMoneyDepositLeft.add(txtDepositDate,"cell 2 12");
    splitPaneMoneyDepositLeft.add(btnSaveDeposit,"cell 20 1");
    splitPaneMoneyDepositLeft.add(btnModifyDeposit,"cell 20 5");
    splitPaneMoneyDepositLeft.add(btnDeleteDeposit,"cell 20 10");
    splitPaneMoneyDepositRight.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    splitPaneMoneyDepositRight.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    splitPaneMoneyDepositRight.setBackground(Color.LIGHT_GRAY);
    splitPaneMoneyDeposit.setLeftComponent(splitPaneMoneyDepositLeft);
    splitPaneMoneyDeposit.setRightComponent(splitPaneMoneyDepositRight);
    
  //add money deposit Action listener and database connectivity
    
    btnSaveDeposit.addActionListener(new ActionListener(){
     	public void actionPerformed(ActionEvent arg0){
     	   lblTripTitle.setText("kuch to hua10");
     		
     			
     		
     		
     		}

 	});
     
     btnModifyDeposit.addActionListener(new ActionListener(){
     	public void actionPerformed(ActionEvent arg0){
     	   lblTripTitle.setText("kuch to hua11");
     		
     			
     		
     		
     		}

 	});
     
     btnDeleteDeposit.addActionListener(new ActionListener(){
     	public void actionPerformed(ActionEvent arg0){
     	   lblTripTitle.setText("kuch to hua12");
     		
     			
     		
     		
     		}

 	});
    
    /* //expenditure panel functionality and property
     panelExpend.setLayout(new MigLayout());
     panelExpend.setBackground(Color.LIGHT_GRAY);
     panelExpend.add(lblExpendTitle,"cell 3 0");
     panelExpend.add(lblTripID,"cell 0 2");
     panelExpend.add(comboBoxTripID,"cell 2 2");
     panelExpend.add(lblTripName,"cell 0 4");
     panelExpend.add(txtTripName,"cell 2 4");
     panelExpend.add(lblExpendDate,"cell 0 6");
     panelExpend.add(txtExpendDate,"cell 2 6");
     panelExpend.add(lblExpendAmt,"cell 0 8");
     panelExpend.add(txtExpendAmt,"cell 2 8");
     panelExpend.add(lblExpendDescription,"cell 0 10");
     panelExpend.add(txtExpendDescription,"cell 2 10 1 3");
     panelExpend.add(btnSaveExpend,"cell 7 3");
     panelExpend.add(btnModifyExpend,"cell 7 7");
     panelExpend.add(btnDeleteExpend,"cell 7 11");
     
     //add money deposit Action listener and database connectivity
     
     btnSaveExpend.addActionListener(new ActionListener(){
      	public void actionPerformed(ActionEvent arg0){
      	   lblTripTitle.setText("kuch to hua13");
      		
      			
      		
      		
      		}

  	});
      
      btnModifyExpend.addActionListener(new ActionListener(){
      	public void actionPerformed(ActionEvent arg0){
      	   lblTripTitle.setText("kuch to hua14");
      		
      			
      		
      		
      		}

  	});
      
      btnDeleteExpend.addActionListener(new ActionListener(){
      	public void actionPerformed(ActionEvent arg0){
      	   lblTripTitle.setText("kuch to hua15");
      		
      			
      		
      		
      		}

  	}); */
     
     
     
     
     
   
    
   
    
    //adding "add new trip panel"+"add new member"+"Trip member panel"+"money deposit" to tabbed pane 
    tabpane.add("ADD_NEW_TRIP",panelAddNewTrip);
    tabpane.add("ADD_NEW_MEMBER",panelAddNewMembr);
    tabpane.add("TRIP_MEMBERS",splitPaneTripMembr);
    tabpane.add("DEPOSIT_MONEY",splitPaneMoneyDeposit);
    //tabpane.add("EXPENDITURE",panelExpend);
    //adding tabbed pane to jframe and jframe properties
    add(tabpane);
    pack();
    setIconImage(img.getImage());
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setVisible(true);        
	setTitle("TRIP_CALCULATOR");
	
	
    
  
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   }
	//Main function
    public static void main(String args[]) {
    	TripRogh trip=new TripRogh();
    	
	}
    public void getMembers(){
    	
    	
    }
  
}
